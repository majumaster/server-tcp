import java.io.*;
import java.net.*;

public class TCPClient {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 1234);

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            // read file JSON
            InputStream inputStream = TCPClient.class.getResourceAsStream("resources/example.json");
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder jsonBuilder = new StringBuilder();
            String line;
            while ((line = fileReader.readLine()) != null) {
                jsonBuilder.append(line);
            }
            fileReader.close();

            String json = jsonBuilder.toString();
            out.println(json);

            String response = in.readLine();
            System.out.println("Response from the server: " + response);

            socket.close(); // close connection
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}