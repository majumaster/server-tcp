import java.io.*;
import java.net.*;

public class TCPServer {
    static int port=1234;
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(port); // Creating a server socket on port 1234
            System.out.println("The server is listening on port "+port+"...");

            while (true) {
                Socket clientSocket = serverSocket.accept(); // Accepting a connection from the client
                System.out.println("Connected to the client: " + clientSocket.getInetAddress().getHostAddress());

                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

                String message = in.readLine(); // Reading a message from the client

                String filePath = "datain.json";

                saveJson(new File(filePath),message);

                System.out.println("Message from client: " + message);

                out.println("The server received a message: " + message); // Sending a response to the client

                clientSocket.close(); // close connection
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void saveJson(File file, String message){
        try {
            if (!file.exists()) {
                file.createNewFile();
                System.out.println("The JSON file has been created.");
            }

            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(message);
            fileWriter.close();

            System.out.println("The JSON file has been successfully saved.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}